import { ref } from "vue"
import { defineStore } from "pinia"
import type User from "@/types/User"
import axios from "axios"

export const useUserStore = defineStore("user", () => {
  const users = ref<User[]>([
    {id:1 , username: "admin1", name: "admin 1", password: "12345678"},
    {id:2 , username: "admin2", name: "admin 2", password: "87654321"}
  ])
  
  return {users };
})