import { computed, ref } from 'vue'
import { defineStore } from 'pinia'
import router from '@/router'

export const useLoginStore = defineStore('login', () => {
  const userName = ref('')
  const isLogin = computed(() => {
    return userName.value !== ''
  })
  const login = (loginName: string) => {
    userName.value = loginName
    localStorage.setItem('userName', loginName)
    router.push("/deposit-view");
  }
  const logout = () => {
    userName.value = ''
    localStorage.removeItem('userName')
  }
  const loadData = () => {
    userName.value = localStorage.getItem('userName') || ''
  }
  return { userName, isLogin, login, logout, loadData }
})
